![Swirling Infestation](https://bitbucket.org/ahigerd/ld42/raw/master/Assets/Images/title.png)

Ludum Dare 42
-------------
**Theme:** Running Out of Space

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland)

Swirling Infestation
====================
Fight back against the curse that is spiraling into the town! It is a dark magic, contagious, spreading to other victims with no more than a touch.

You are the town's only hope, a sorceress with the power to drive off the curse.

You have three powers available to you:

* **Pulse**: A forceful burst that can push the curse out of its victims, leaving the physical world untouched. (Costs 1 wheel)
* **Radar**: Reach out to find the souls of the afflicted. (Drains energy when active)
* **Teleport**: An emergency escape. You have no control over the destination, but the curse can't follow you. (Costs 2 wheels)

Be cautious! You are strong enough to resist the curse, but you are not immune to it. While you are cursed, you still have access to all of your
powers, but the strain of fighting its influence will quickly drain your vitality. Pulse will expel the curse from your body as easily as from
the townspeople, and Teleport will leave the curse behind.

Once cast out of the body, the curse loses its virulence, but its frustrated power becomes an impassible maelstrom. Be wary of where you purge
the curse, as the maelstrom will slowly grow, blocking you off and leaving you running out of space.

Downloads
---------
* [Windows (64-bit)](https://bitbucket.org/ahigerd/ld42/downloads/SwirlingInfestation-Win64.zip)
* [Windows (32-bit)](https://bitbucket.org/ahigerd/ld42/downloads/SwirlingInfestation-Win32.zip)
* [macOS](https://bitbucket.org/ahigerd/ld42/downloads/SwirlingInfestation-macOS.zip)
* [Linux](https://bitbucket.org/ahigerd/ld42/downloads/SwirlingInfestation-Linux.tar.bz2)

Controls
--------
* Move: Arrow Keys
* Pulse: Space
* Radar: Z
* Teleport: X
* Pause: Escape

Credits
-------
Programming: [Adam Higerd](https://ldjam.com/users/coda-highland)

Some library functions: [Austin Allman](https://ldjam.com/users/drazil100)

Sound effects and design: [Adam Higerd](https://ldjam.com/users/coda-highland)

Visual effects and animations: [Adam Higerd](https://ldjam.com/users/coda-highland)

Character sprites: [Aeon Warriors Field & Battle Sprites](https://opengameart.org/content/js-actors-aeon-warriors-field-battle-sprites) by JosephSeraph

Tilesets derived from: [Lots of free 2d tiles and sprites](https://opengameart.org/content/lots-of-free-2d-tiles-and-sprites-by-hyptosis) by Hyptosis

Background music: Various pieces from [Soundimage.org](http://soundimage.org) by Eric Matyas

License
=======
Copyright (c) 2018 Adam Higerd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
