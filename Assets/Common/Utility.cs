using UnityEngine;

class Utility {
  public static float Approach(float current, float target, float rate, float deadZone = 0.1f) {
    if (Mathf.Abs(current - target) < deadZone)
      return target;
    float time = Time.deltaTime;
    float steps = time * 60;
    return (current - target) * Mathf.Pow(0.5f, rate * steps) + target;
  }

  public static Vector2 RotateVector(Vector2 vec, float degrees) {
    float rad = degrees * Mathf.Deg2Rad;
    float cos = Mathf.Cos(rad);
    float sin = Mathf.Sin(rad);
    return new Vector2(
        vec.x * cos + vec.y * sin,
        vec.y * cos - vec.x * sin
    );
  }

  public static Vector2 RotateVectorRad(Vector2 vec, float rad) {
    float cos = Mathf.Cos(rad);
    float sin = Mathf.Sin(rad);
    return new Vector2(
        vec.x * cos + vec.y * sin,
        vec.y * cos - vec.x * sin
    );
  }

  public static int OppositeSide(int side) {
    return (side + 2) % 4;
  }

  public static Vector2 SideNormal(int side) {
    switch (side) {
      case 0: return Vector2.left;
      case 1: return Vector2.down;
      case 2: return Vector2.right;
      default: return Vector2.up;
    }
  }

  public static Vector2 SideAxis(int side) {
    return SideNormal((side + 1) % 4);
  }

  public static void Log(string format, params object[] args) {
    Debug.Log(string.Format(format, args));
  }

  public static void DrawDebugRect(Vector2 bottomLeft, Vector2 topRight) {
    Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
    Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);
    Debug.DrawLine(bottomLeft, bottomRight, Color.red, 0, false);
    Debug.DrawLine(bottomRight, topRight, Color.green, 0, false);
    Debug.DrawLine(topRight, topLeft, Color.blue, 0, false);
    Debug.DrawLine(topLeft, bottomLeft, Color.yellow, 0, false);
  }
}

