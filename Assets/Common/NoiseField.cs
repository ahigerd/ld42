﻿using UnityEngine;
using System;
using System.Collections;

public class NoiseField {
	private static double Dot(int[] g, double x, double y, double z) {
		return g[0] * x + g[1] * y + g[2] * z;
	}

	public static uint Hash(int x, int y, int z, uint seedToUse)
	{
		// based on Jenkins96 hash
		uint a = (uint)(0x9e3779b9 + x - z << 8);
		uint b = (uint)(0x9e3779b9 + y + z << 8);
		uint c = seedToUse;

		a -= b; a -= c; a ^= (c>>13);
		b -= c; b -= a; b ^= (a<<8);
		c -= a; c -= b; c ^= (b>>13);
		a -= b; a -= c; a ^= (c>>12);
		b -= c; b -= a; b ^= (a<<16);
		c -= a; c -= b; c ^= (b>>5);
		a -= b; a -= c; a ^= (c>>3);
		b -= c; b -= a; b ^= (a<<10);
		c -= a; c -= b; c ^= (b>>15);

		c += 12;

		a -= b; a -= c; a ^= (c>>13);
		b -= c; b -= a; b ^= (a<<8);
		c -= a; c -= b; c ^= (b>>13);
		a -= b; a -= c; a ^= (c>>12);
		b -= c; b -= a; b ^= (a<<16);
		c -= a; c -= b; c ^= (b>>5);
		a -= b; a -= c; a ^= (c>>3);
		b -= c; b -= a; b ^= (a<<10);
		c -= a; c -= b; c ^= (b>>15);

		return c & 0xFFFFFFFF;
	}

	private static readonly int[][] grad3 = {
		new int[] { 1, 1, 0},
		new int[] {-1, 1, 0},
		new int[] { 1,-1, 0},
		new int[] {-1,-1, 0},
		new int[] { 1, 0, 1},
		new int[] {-1, 0, 1},
		new int[] { 1, 0,-1},
		new int[] {-1, 0,-1},
		new int[] { 0, 1, 1},
		new int[] { 0,-1, 1},
		new int[] { 0, 1,-1},
		new int[] { 0,-1,-1}
	};

	public static double noise(double xin, double yin, double zin, uint seed, double scale = 1.0) {
		xin /= scale;
		yin /= scale;
		zin /= scale;
		double n0, n1, n2, n3;

		//skew input space
		double s = (xin + yin + zin) / 3.0;
		int i = (int)Math.Floor(xin + s);
		int j = (int)Math.Floor(yin + s);
		int k = (int)Math.Floor(zin + s);
		double t = (i + j + k) / 6.0;

		// unskew
		double X0 = i - t;
		double Y0 = j - t;
		double Z0 = k - t;
		// x,y,z distance from cell origin
		double x0 = xin - X0;
		double y0 = yin - Y0;
		double z0 = zin - Z0;

		// offsets for two reference corners
		int i1, j1, k1;
		int i2, j2, k2;
		if(x0 > y0) {
			if(y0 > z0) {
				i1 = 1;
				j1 = 0;
				k1 = 0;
				i2 = 1;
				j2 = 1;
				k2 = 0;
			} else if(x0 > z0) {
				i1 = 1;
				j1 = 0;
				k1 = 0;
				i2 = 1;
				j2 = 0;
				k2 = 1;
			} else {
				i1 = 0;
				j1 = 0;
				k1 = 1;
				i2 = 1;
				j2 = 0;
				k2 = 1;
			}
		} else {
			if(y0 < z0) {
				i1 = 0;
				j1 = 0;
				k1 = 1;
				i2 = 0;
				j2 = 1;
				k2 = 1;
			} else if(x0 < z0) {
				i1 = 0;
				j1 = 1;
				k1 = 0;
				i2 = 0;
				j2 = 1;
				k2 = 1;
			} else {
				i1 = 0;
				j1 = 1;
				k1 = 0;
				i2 = 1;
				j2 = 1;
				k2 = 0;
			}
		}

		// other corner offsets unskewed
		double x1 = x0 - i1  + 1.0/6.0;
		double y1 = y0 - j1  + 1.0/6.0;
		double z1 = z0 - k1  + 1.0/6.0;
		double x2 = x0 - i2  + 1.0/3.0;
		double y2 = y0 - j2  + 1.0/3.0;
		double z2 = z0 - k2  + 1.0/3.0;
		double x3 = x0 - 1.0 + 1.0/2.0;
		double y3 = y0 - 1.0 + 1.0/2.0;
		double z3 = z0 - 1.0 + 1.0/2.0;

		// generate pseudorandom values for corners
		uint gi0 = Hash(i, j, k, seed) % 12;
		uint gi1 = Hash(i+i1, j+j1, k+k1, seed) % 12;
		uint gi2 = Hash(i+i2, j+j2, k+k2, seed) % 12;
		uint gi3 = Hash(i+1, j+1, k+1, seed) % 12;

		// calculate contributions from 3 corners
		double t0 = 0.5 - x0 * x0 - y0 * y0 - z0 * z0;
		if(t0 < 0) {
			n0 = 0.0;
		} else {
			t0 *= t0;
			n0 = t0 * t0 * Dot(grad3[gi0], x0, y0, z0);
		}

		double t1 = 0.5 - x1 * x1 - y1 * y1 - z1 * z1;
		if(t1 < 0) {
			n1 = 0.0;
		} else {
			t1 *= t1;
			n1 = t1 * t1 * Dot(grad3[gi1], x1, y1, z1);
		}

		double t2 = 0.5 - x2 * x2 - y2 * y2 - z2 * z2;
		if(t2 < 0) {
			n2 = 0.0;
		} else {
			t2 *= t2;
			n2 = t2 * t2 * Dot(grad3[gi2], x2, y2, z2);
		}

		double t3 = 0.5 - x3 * x3 - y3 * y3 - z3 * z3;
		if(t3 < 0) {
			n3 = 0.0;
		} else {
			t3 *= t3;
			n3 = t3 * t3 * Dot(grad3[gi3], x3, y3, z3);
		}

		return (n0 + n1 + n2 + n3) * 32.0;
	}

	private uint seed;
	private int octaves;
	private double scale, depth;

	public NoiseField(uint seed, int octaves = 1, double scale = 32.0, double depth = 7000.0) {
		this.seed = seed;
		this.octaves = octaves;
		this.scale = scale;
		this.depth = depth;
	}

	public double ValueAt(double x, double y, double z) {
		double result = 0;
		double oScale = scale;
		double oDepth = depth;
		for(uint i = 0; i < octaves; i++) {
			result += noise(x, y, z, seed + i, oScale) * oDepth;
			oScale *= 2;
			oDepth /= 2;
		}
		return result;
	}
}
