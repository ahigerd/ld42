using UnityEngine;
using System.Collections.Generic;

public class PulseBehaviour : MonoBehaviour {
  public PointText pointsPrefab = null;
  public Hazard hazardPrefab = null;
  private Material material;
  private float age = -.25f;
  private int combo = 0;

  void Start() {
    transform.localScale = Vector3.zero;
    material = gameObject.GetComponent<MeshRenderer>().material;
  }

  void Update() {
    age += Time.deltaTime;
    if (age < 0) return;
    material.color = new Color(1f, 1f, 1f, Mathf.Clamp(age / 1.5f, 0, 1));
    float colorScale = Mathf.Clamp(age / 4f, 0, 1);
    material.SetColor("_EmissionColor", new Color(0f, .75f * colorScale, .75f * colorScale));
    Vector3 newScale;
    if (transform.localScale.magnitude < 1) {
      newScale = transform.localScale + Vector3.one * Time.deltaTime;
    } else {
      newScale = transform.localScale * (1 + Time.deltaTime * 5);
    }
    transform.localScale = newScale;
    float size = newScale.x / 2;

    List<CharacterCore> targets = new List<CharacterCore>();
    foreach (CharacterCore other in InfestationManager.infestedChars) {
      Vector2 dist = other.transform.position - transform.position;
      if (dist.magnitude < size) {
        targets.Add(other);
      }
    }
    foreach (CharacterCore other in targets) {
      if (other.tag != "Player") {
        combo += 10;
        GameManager.score += combo;
        PointText pt = Instantiate(pointsPrefab, other.transform);
        pt.Text = "" + combo;
      }
      InfestationManager.Purge(other);
      Vector2 center = (Vector2)other.transform.position - new Vector2(0, .5f);
      Hazard hazard = Instantiate(hazardPrefab, center, Quaternion.identity).GetComponent<Hazard>();
      hazard.trajectory = (center - (Vector2)transform.position).normalized * 4f;
      hazard.trajectory.y *= -1;
      hazard.source = other;
    }

    if (age > 1.4f) {
      Destroy(gameObject);
    }
  }
}
