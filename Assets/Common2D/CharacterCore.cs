using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CharacterCore : LevelAwareBehaviour
{
  protected static string[] walkAnim = new string[] { "Left", "Down", "Right", "Up" };
  protected static string[] idleAnim = new string[] { "IdleLeft", "IdleDown", "IdleRight", "IdleUp" };

  public static float PIXEL = 1f / 16f;
  public static float GRAVITY = 20f;
  public float speed = 4f;
  public float layer = 0f;
  public Vector2 hitBoxSize = new Vector2(0.6f, 0.8f);
  public AudioClip deathSound = null;
  protected AudioSource audioSource;
  public int facing = 1;
  public bool isWalking = false;
  public bool isInfested = false;
  public bool isInfectious = true;
  public bool disableCollision = false;

  private TileMap _tileMap = null;
  public TileMap tileMap
  {
    get
    {
      if (_tileMap == null) {
        _tileMap = GameObject.Find("Map").GetComponent<TileMap>();
      }
      return _tileMap;
    }
  }

  public Vector2 origin
  {
    get
    {
      return WorldToMapPoint(transform.position);
    }
    set
    {
      transform.position = MapToWorldPoint(value, layer);
    }
  }

  public Vector2 midpoint
  {
    get
    {
      return origin - new Vector2(0, hitBoxSize.y / 2f);
    }
  }

  public Rect hitBox
  {
    get
    {
      return HitBoxAt(origin);
    }
  }

  public static Vector2 WorldToMapPoint(Vector3 point)
  {
    return new Vector2(point.x, -point.y);
  }

  public static Vector3 MapToWorldPoint(Vector2 point, float layer = 0f)
  {
    return new Vector3(point.x, -point.y, layer - point.y / 100000f);
  }

  public Rect HitBoxAt(Vector2 pos)
  {
    return new Rect(pos.x - hitBoxSize.x / 2f, pos.y - hitBoxSize.y, hitBoxSize.x, hitBoxSize.y);
  }

  public void SetFacing(Vector2 dir, bool walking) {
    int newFacing = facing;
    if (dir.magnitude > 0) {
      float degrees = Vector2.SignedAngle(Vector2.right, dir);
      if (degrees > -40 && degrees < 40) {
        newFacing = 2;
      } else if (degrees > 50 && degrees < 130) {
        newFacing = 1;
      } else if (degrees > 140 || degrees < -140) {
        newFacing = 0;
      } else if (degrees > -130 && degrees < -50) {
        newFacing = 3;
      } else if (degrees >= 30 && degrees <= 60) {
        if (facing == 0 || facing == 3) {
          newFacing = degrees > 45 ? 1 : 2;
        }
      } else if (degrees >= 120 && degrees <= 150) {
        if (facing == 3 || facing == 2) {
          newFacing = degrees > 135 ? 0 : 1;
        }
      } else if (degrees >= -60 && degrees <= -30) {
        if (facing == 0 || facing == 1) {
          newFacing = degrees > -45 ? 2 : 3;
        }
      } else if (degrees >= -150 && degrees <= -120) {
        if (facing == 2 || facing == 1) {
          newFacing = degrees > -135 ? 3 : 0;
        }
      }
    }
    if (!walking) {
      gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation(idleAnim[newFacing], true);
    } else if (facing != newFacing || !isWalking) {
      gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation(walkAnim[newFacing], true);
    }
    facing = newFacing;
    isWalking = walking;
  }

  public virtual void Start()
  {
    audioSource = gameObject.AddComponent<AudioSource>();
    SpriteAnimationManager sam = gameObject.GetComponent<SpriteAnimationManager>() as SpriteAnimationManager;
    if (sam) {
      sam.SwitchAnimation("Idle", true);
    }
  }

  public virtual bool BlockedAt(Vector2 v)
  {
    Rect newHB = HitBoxAt(v);
    if (tileMap.IsSolid(newHB.xMax + PIXEL, newHB.yMax - PIXEL) ||
        tileMap.IsSolid(newHB.xMax + PIXEL, newHB.yMin) ||
        tileMap.IsSolid(newHB.xMin - PIXEL, newHB.yMax - PIXEL) ||
        tileMap.IsSolid(newHB.xMin - PIXEL, newHB.yMin)) {
      return true;
    }
    foreach (LevelAwareBehaviour lb in GameManager.behaviours) {
      CharacterCore other = lb.gameObject.GetComponent<CharacterCore>();
      if (other == null || other == this || other.disableCollision) continue;
      Rect ohb = other.hitBox;
      if (ohb.Overlaps(newHB)) return false;
    }
    return false;
  }

  public virtual Vector2 Move(Vector2 v)
  {
    Vector2 delta = v * Time.deltaTime;
    Vector2 target = origin + delta;

    Rect oldHB = hitBox;
    Rect newHB = HitBoxAt(target);

    // First sweep horizontal motion
    if (v.x > 0 && (tileMap.IsSolid(newHB.xMax + PIXEL, oldHB.yMax - PIXEL) || tileMap.IsSolid(newHB.xMax + PIXEL, oldHB.yMin))) {
      v.y += Mathf.Sign(v.y) * v.x;
      v.x = 0;
      target.x = Mathf.Floor(newHB.xMax + PIXEL) - PIXEL - hitBoxSize.x / 2;
    } else if (v.x < 0 && (tileMap.IsSolid(newHB.xMin - PIXEL, oldHB.yMax - PIXEL) || tileMap.IsSolid(newHB.xMin - PIXEL, oldHB.yMin))) {
      v.y -= Mathf.Sign(v.y) * v.x;
      v.x = 0;
      target.x = Mathf.Ceil(newHB.xMin - PIXEL) + PIXEL + hitBoxSize.x / 2;
    }

    // Then sweep vertical motion
    if (v.y > 0 && (tileMap.IsSolid(oldHB.xMin, newHB.yMax + PIXEL) || tileMap.IsSolid(oldHB.xMax, newHB.yMax + PIXEL))) {
      v.x += Mathf.Sign(v.x) * v.y;
      target.y = Mathf.Floor(target.y + PIXEL);
      v.y = 0;
    } else if (v.y < 0 && (tileMap.IsSolid(oldHB.xMin, newHB.yMin - PIXEL) || tileMap.IsSolid(oldHB.xMax, newHB.yMin - PIXEL))) {
      v.x -= Mathf.Sign(v.x) * v.y;
      target.y = Mathf.Ceil(target.y - hitBoxSize.y - PIXEL) + PIXEL + hitBoxSize.y;
      v.y = 0;
    }

    if (disableCollision) {
      origin = target;
      return v;
    }

    // Evaluate collisions with other characters next
    if (v.magnitude > 0) {
      newHB = HitBoxAt(target);
      foreach (LevelAwareBehaviour lb in GameManager.behaviours) {
        CharacterCore other = lb.gameObject.GetComponent<CharacterCore>();
        if (other == null || other == this || other.disableCollision) continue;
        Rect ohb = other.hitBox;
        if (!ohb.Overlaps(newHB)) continue;
        float xOverlap = v.x > 0 ? (newHB.xMax - ohb.xMin) : (ohb.xMax - newHB.xMin);
        float yOverlap = v.y > 0 ? (newHB.yMax - ohb.yMin) : (ohb.yMax - newHB.yMin);
        if (xOverlap > yOverlap) {
          target.y += v.y > 0 ? -yOverlap : yOverlap;
          v.y = 0;
        } else {
          target.x += v.x > 0 ? -xOverlap : xOverlap;
          v.x = 0;
        }
        OnCharacterCollision(other);
        other.OnCharacterCollision(this);
        if (v.x == 0 && v.y == 0) break;
        newHB = HitBoxAt(target);
      }
    }

    origin = target;

    return v;
  }

  public void OnDrawGizmos()
  {
    Utility.DrawDebugRect(MapToWorldPoint(hitBox.min), MapToWorldPoint(hitBox.max));
  }

  public void Kill(bool destroy)
  {
    // Use the player's audio source because this one might get destroyed
    PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    player.audioSource.PlayOneShot(deathSound);
    if (destroy) {
      Destroy(gameObject);
    }
  }

  public virtual void OnCharacterCollision(CharacterCore other) {
    if (isInfectious && isInfested && !other.isInfested && other.isInfectious) {
      InfestationManager.Infest(other);
    }
  }
}
