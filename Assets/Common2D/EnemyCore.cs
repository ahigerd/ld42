using UnityEngine;

public class EnemyCore : CharacterCore {
	public int minLevel = 1;
	public int maxLevel = -1;
	public int health = 1;
	public int pointValue = 100;
  public float speedFactor = 1;
  public float speedTime = 0;
  public Vector2 velocity = Vector2.zero;

  private float walkTime = 0;
  private float takeStep = 0;
  private Vector2 stepDirection = Vector2.zero;

	public virtual void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Bullet") {
			Destroy(coll.gameObject);
			health--;
			if (health <= 0) {
				Kill(true);
				GameManager.score += pointValue;
			}
		}
	}

  void Update() {
    if (speedTime > 0) {
      speedTime -= Time.deltaTime;
    } else if (speedTime <= 0) {
      speedFactor = 1;
    }

    Vector2 toPlayer = Vector2.zero;
    Vector2 dir = Vector2.zero;

    if (isInfested) {
      GameObject playerGO = GameObject.FindWithTag("Player");
      PlayerController player = playerGO == null ? null : playerGO.GetComponent<PlayerController>();
      if (player != null && GameManager.health > 0) {
        toPlayer = player.transform.position - transform.position;
        if (player.isInfested) {
          if (toPlayer.magnitude > 3) {
            toPlayer = Vector2.positiveInfinity;
          } else {
            toPlayer *= -1;
            speedTime = .1f;
            speedFactor = .2f;
          }
        }
      } else {
        toPlayer = Vector2.positiveInfinity;
      }

      Vector2 target = toPlayer.magnitude < 15 ? toPlayer : Vector2.positiveInfinity;
      Vector2 adjustments = Vector2.zero;
      foreach (LevelAwareBehaviour lb in GameManager.behaviours) {
        CharacterCore other = lb.GetComponent<CharacterCore>();
        if (other == null) continue;
        Vector2 dist = other.transform.position - transform.position;
        if (other.isInfested) {
          if (dist.magnitude < 1.5f) {
            // Nudge infested characters subtly away from each other if they get too close
            adjustments += dist;
          }
        } else if (dist.magnitude < target.magnitude && dist.magnitude < 6) {
          target = dist;
        }
      }
      if (target.magnitude < 15) {
        dir = target.normalized - (adjustments.normalized * .5f);
      }
    } else {
      Vector2 danger = Vector2.positiveInfinity;
      foreach (CharacterCore other in InfestationManager.infestedChars) {
        Vector2 dist = other.transform.position - transform.position;
        if (dist.magnitude < danger.magnitude) {
          danger = dist;
        }
      }
      if (danger.magnitude < 7) {
        dir = -danger;
        speedTime = .5f;
        speedFactor = .7f;
      }
    }

    if (dir != Vector2.zero) {
      takeStep = 0;
    } else if (velocity == Vector2.zero && (isInfested || UnityEngine.Random.value < .01f)) {
      takeStep = 2f;
      TileMap map = GameObject.FindWithTag("Map").GetComponent<TileMap>();
      int retry = 10;
      do {
        float rand = UnityEngine.Random.value;
        if (rand < .25f) {
          stepDirection = Vector2.left;
        } else if (rand < .5f) {
          stepDirection = Vector2.down;
        } else if (rand < .75f) {
          stepDirection = Vector2.right;
        } else {
          stepDirection = Vector2.up;
        }
        --retry;
      } while (retry > 0 && map.IsSolid(transform.position.x + stepDirection.x, -transform.position.y - stepDirection.y));
    }
    if (takeStep > 0) {
      takeStep -= Time.deltaTime;
      dir = stepDirection;
      speedFactor = .2f;
      speedTime = .1f;
    }

    dir.y *= -1;
    if (Vector2.Angle(dir, velocity) < 90) {
      dir = Vector3.RotateTowards(velocity, dir.normalized * speed * speedFactor, 4 * Mathf.PI * Time.deltaTime, 2 * speed * speedFactor * Time.deltaTime);
    } else if (dir.magnitude > .05f) {
      dir = Vector2.MoveTowards(velocity, dir.normalized * speed * speedFactor, 8 * speed * speedFactor * Time.deltaTime);
    } else {
      dir = Vector2.MoveTowards(velocity, Vector2.zero, 8 * speed * speedFactor * Time.deltaTime);
    }
    dir = Vector2.ClampMagnitude(dir, speed * speedFactor);
    if (dir != Vector2.zero) {
      dir = Move(dir);
      SetFacing(dir, true);
      walkTime = .3f; // keep animating for 450ms after no longer moving
    } else if (walkTime > 0) {
      walkTime -= Time.deltaTime;
      SetFacing(Vector2.zero, true);
    } else {
      SetFacing(Vector2.zero, false);
    }
    velocity = dir;
  }

  public override void OnCharacterCollision(CharacterCore other) {
    bool wasInfested = isInfested;
    if (wasInfested && !other.isInfested) {
      // When passing off an infection, slow down for a moment to give them time to spread out
      speedTime = .5f;
      speedFactor = .7f;
    }
    base.OnCharacterCollision(other);
    if (!wasInfested && isInfested) {
      // When first infected, get a burst of speed
      speedTime = 1f;
      speedFactor = 1.5f;
    }
  }
}
