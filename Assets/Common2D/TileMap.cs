using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class TileMap : LevelAwareBehaviour {
  public static int MAP_WIDTH = 126;
  public static int MAP_HEIGHT = 126;
	private static float BACKGROUND_DEPTH = 0.3f;
	private static float TERRAIN_DEPTH = -0.1f;

	public List<EnemyCore> enemyPrefabs = new List<EnemyCore>();
	public List<EnemyCore> collectPrefabs = new List<EnemyCore>();
  private List<EnemyCore> enemies = new List<EnemyCore>();

	public Material material;
	public Texture2D backgroundTexture;
	private Material backgroundMaterial;
	private Mesh backgroundMesh;
  private Mesh overlayMesh;

  private byte[,] tiles = new byte[MAP_WIDTH, MAP_HEIGHT];
	private static float tileSizeH = 1f / 32f;
	private static float tileSizeV = 1f / 16f;
	private static Vector2 TileToUV(int x, int y) {
		return new Vector2(x * tileSizeH, (15 - y) * tileSizeV);
	}
  // TODO: This will need updated on a per-application basis
	private static Vector2[] roadUV = {
		/* .0. 0*0 .0. */ TileToUV(1, 10),
		/* .0. 0*0 .1. */ TileToUV(1, 11),
		/* .0. 0*1 .0. */ TileToUV(2, 10),
		/* .0. 0*1 .1. */ TileToUV(2, 11),
		/* .0. 1*0 .0. */ TileToUV(0, 10),
		/* .0. 1*0 .1. */ TileToUV(0, 11),
		/* .0. 1*1 .0. */ TileToUV(0, 12), // TODO: make tile if needed
		/* .0. 1*1 .1. */ TileToUV(1, 10), // "
		/* .1. 0*0 .0. */ TileToUV(1, 9),
		/* .1. 0*0 .1. */ TileToUV(0, 12), // "
		/* .1. 0*1 .0. */ TileToUV(2, 9),
		/* .1. 0*1 .1. */ TileToUV(1, 10), // "
		/* .1. 1*0 .0. */ TileToUV(0, 9),
		/* .1. 1*0 .1. */ TileToUV(1, 10), // "
		/* .1. 1*1 .0. */ TileToUV(0, 12), // "
		/* .1. 1*1 .1. */ TileToUV(1, 10), // "

		/* 0:0 :*: 0:0 */ TileToUV(1, 10), // TODO: is this tile needed?
		/* 0:0 :*: 0:1 */ TileToUV(4, 10),
		/* 0:0 :*: 1:0 */ TileToUV(3, 10),
		/* 0:0 :*: 1:1 */ TileToUV(1, 10), // TODO: make tile if needed
		/* 0:1 :*: 0:0 */ TileToUV(4, 9),
		/* 0:1 :*: 0:1 */ TileToUV(1, 10), // "
		/* 0:1 :*: 1:0 */ TileToUV(1, 10), // "
		/* 0:1 :*: 1:1 */ TileToUV(1, 10), // "
		/* 1:0 :*: 0:0 */ TileToUV(3, 9),
		/* 1:0 :*: 0:1 */ TileToUV(1, 10), // "
		/* 1:0 :*: 1:0 */ TileToUV(1, 10), // "
		/* 1:0 :*: 1:1 */ TileToUV(1, 10), // "
		/* 1:1 :*: 0:0 */ TileToUV(1, 10), // "
		/* 1:1 :*: 0:1 */ TileToUV(1, 10), // "
		/* 1:1 :*: 1:0 */ TileToUV(1, 10), // "
		/* 1:1 :*: 1:1 */ TileToUV(1, 10), // "
  };

  private static Vector2[] otherTiles = {
    TileToUV(0, 0), // grass
    TileToUV(18, 10), // alt grass 1
    TileToUV(19, 10), // alt grass 2
    TileToUV(8, 14), // closed door top
    TileToUV(8, 15), // closed door bottom
    TileToUV(6, 12), // open door top
    TileToUV(6, 13), // open door bottom
	};

  private static Vector2[] lightBrownPalette = {
    TileToUV( 0,  5), // ground left wall
    TileToUV( 1,  5), // ground mid wall
    TileToUV( 2,  5), // ground right wall
    TileToUV( 0,  4), // mid left wall
    TileToUV( 1,  4), // mid mid wall
    TileToUV( 2,  4), // mid right wall
    TileToUV( 3,  4), // top left wall
    TileToUV( 4,  4), // top mid wall
    TileToUV( 5,  4), // top right wall
    TileToUV( 8, 13), // front left roof
    TileToUV( 9, 13), // front mid roof
    TileToUV(10, 13), // front right roof
    TileToUV( 8, 12), // mid left roof
    TileToUV( 9, 12), // mid mid roof
    TileToUV(10, 12), // mid right roof
    TileToUV( 8, 11), // back left roof
    TileToUV( 9, 11), // back mid roof
    TileToUV(10, 11), // back right roof
    TileToUV( 3,  5), // alt wall 1
    TileToUV( 4,  5), // alt wall 2
    TileToUV( 9, 12), // alt roof 1
    TileToUV( 9, 12), // alt roof 1
  };

  private static Vector2[] darkBrownPalette = {
    TileToUV(20,  2), // ground left wall
    TileToUV(21,  2), // ground mid wall
    TileToUV(22,  2), // ground right wall
    TileToUV(20,  1), // mid left wall
    TileToUV(21,  1), // mid mid wall
    TileToUV(22,  1), // mid right wall
    TileToUV(20,  0), // top left wall
    TileToUV(21,  0), // top mid wall
    TileToUV(22,  0), // top right wall
    TileToUV( 8, 13), // front left roof
    TileToUV( 9, 13), // front mid roof
    TileToUV(10, 13), // front right roof
    TileToUV( 8, 12), // mid left roof
    TileToUV( 9, 12), // mid mid roof
    TileToUV(10, 12), // mid right roof
    TileToUV( 8, 11), // back left roof
    TileToUV( 9, 11), // back mid roof
    TileToUV(10, 11), // back right roof
    TileToUV(21,  1), // alt wall 1
    TileToUV(21,  1), // alt wall 2
    TileToUV( 9, 12), // alt roof 1
    TileToUV( 9, 12), // alt roof 1
  };

  private static Vector2[] lightGreenPalette = {
    TileToUV( 0,  5), // ground left wall
    TileToUV( 1,  5), // ground mid wall
    TileToUV( 2,  5), // ground right wall
    TileToUV( 0,  4), // mid left wall
    TileToUV( 1,  4), // mid mid wall
    TileToUV( 2,  4), // mid right wall
    TileToUV( 3,  4), // top left wall
    TileToUV( 4,  4), // top mid wall
    TileToUV( 5,  4), // top right wall
    TileToUV(13, 13), // front left roof
    TileToUV(14, 13), // front mid roof
    TileToUV(15, 13), // front right roof
    TileToUV(13, 12), // mid left roof
    TileToUV(14, 12), // mid mid roof
    TileToUV(15, 12), // mid right roof
    TileToUV(13, 11), // back left roof
    TileToUV(14, 11), // back mid roof
    TileToUV(15, 11), // back right roof
    TileToUV( 3,  5), // alt wall 1
    TileToUV( 4,  5), // alt wall 2
    TileToUV(11, 11), // alt roof 1
    TileToUV(12, 11), // alt roof 1
  };

  private static Vector2[] darkGreenPalette = {
    TileToUV(20,  2), // ground left wall
    TileToUV(21,  2), // ground mid wall
    TileToUV(22,  2), // ground right wall
    TileToUV(20,  1), // mid left wall
    TileToUV(21,  1), // mid mid wall
    TileToUV(22,  1), // mid right wall
    TileToUV(20,  0), // top left wall
    TileToUV(21,  0), // top mid wall
    TileToUV(22,  0), // top right wall
    TileToUV(13, 13), // front left roof
    TileToUV(14, 13), // front mid roof
    TileToUV(15, 13), // front right roof
    TileToUV(13, 12), // mid left roof
    TileToUV(14, 12), // mid mid roof
    TileToUV(15, 12), // mid right roof
    TileToUV(13, 11), // back left roof
    TileToUV(14, 11), // back mid roof
    TileToUV(15, 11), // back right roof
    TileToUV(21,  1), // alt wall 1
    TileToUV(21,  1), // alt wall 2
    TileToUV(11, 11), // alt roof 1
    TileToUV(12, 11), // alt roof 1
  };

  private static Vector2[][] housePalettes = {
    lightBrownPalette,
    darkBrownPalette,
    lightGreenPalette,
    darkGreenPalette,
  };

	public void GenerateTiles() {
    // TODO: Application-specific code
    for (int y = 0; y < MAP_HEIGHT; y++) {
      for (int x = 0; x < MAP_WIDTH; x++) {
        if (x >= 62 && x <= 67 && y >= 62 && y <= 67) {
          tiles[y, x] = 1;
        } else if (UnityEngine.Random.value < .8) {
          tiles[y, x] = 2;
        } else {
          tiles[y, x] = (byte)UnityEngine.Random.Range(2, 4.999f);
        }
      }
    }
    List<Rect> houses = new List<Rect>();

    // block off the temporary spawning point
    houses.Add(new Rect(62, 62, 5, 5));

    for (int i = 0; i < 80; i++) {
      int p = (int)UnityEngine.Random.Range(0, 3.999f);
      int width, height, xs, ys;
      Rect r;
      do {
        width = (int)UnityEngine.Random.Range(3, 15);
        height = (int)UnityEngine.Random.Range(6, 10);
        xs = UnityEngine.Random.Range(2, MAP_WIDTH - 4 - width);
        ys = UnityEngine.Random.Range(2, MAP_HEIGHT - 4 - height);
        r = new Rect(xs - 1, ys - 1, width + 2, height + 2);
      } while (houses.Any(h => h.Overlaps(r)));
      houses.Add(r);
      for (int y = ys; y < ys + height; y++) {
        int yOff = (ys + height - 1 - y);
        if (yOff < 4) {
          yOff *= 3;
        } else if (yOff == height - 1) {
          yOff = 15;
        } else {
          yOff = 12;
        }
        for (int x = xs; x < xs + width; x++) {
          int xOff = (x == xs) ? 0 : ((x == xs + width - 1) ? 2 : 1);
          if (xOff == 1 && yOff == 3) {
            int alt = UnityEngine.Random.Range(1, 10);
            if (alt == 1) {
              xOff = 15;
            } else if (alt == 2) {
              xOff = 16;
            }
          } else if (xOff == 1 && yOff == 12) {
            int alt = UnityEngine.Random.Range(1, 10);
            if (alt == 1) {
              xOff = 8;
            } else if (alt == 2) {
              xOff = 9;
            }
          }
          tiles[x, y] = (byte)(65 + 22 * p + yOff + xOff);
        }
      }
      int doorPos = (int)UnityEngine.Random.Range(xs + 1, xs + width - 1.999f);
      tiles[doorPos, ys + height - 2] = 5;
      tiles[doorPos, ys + height - 1] = 6;
      tiles[doorPos - 1, ys + height] = 1;
      tiles[doorPos, ys + height] = 1;
      tiles[doorPos + 1, ys + height] = 1;
      tiles[doorPos - 1, ys + height + 1] = 1;
      tiles[doorPos, ys + height + 1] = 1;
      tiles[doorPos + 1, ys + height + 1] = 1;
    }
		PopulateMesh();
	}

	public void Awake()
	{
    if (backgroundMaterial == null) {
      backgroundMaterial = new Material(material);
      backgroundMaterial.mainTexture = backgroundTexture;
    }

    GenerateTiles();
		PopulateMesh();
	}

	public override void OnGameStart() {
		foreach (EnemyCore enemy in enemies) {
			Destroy(enemy.gameObject);
		}
		enemies.Clear();
	}

	public override void OnPlayerSpawn() {
	}

	public void NewLevel() {
    GenerateTiles();
    PopulateMesh();
	}

	public byte TileTypeAt(float x, float y)
	{
		if (x < 0 || y < 0 || x >= MAP_WIDTH || y >= MAP_HEIGHT) {
			return (byte)65;
		}

		return tiles[(int)x, (int)y];
	}

	public bool IsSolid(float x, float y)
	{
		byte tileType = TileTypeAt(x, y);
    if (tileType == 5 || tileType == 6) {
      return true;
    }
    if (tileType < 65) {
      return false;
    }
    int index = (int)(tileType - 65) % 22;
    return index != 15 && index != 16 && index != 17;
	}

	public void PopulateMesh()
	{
    if (overlayMesh == null) {
      overlayMesh = new Mesh();
    }
		overlayMesh.Clear();
    if (backgroundMesh == null) {
      backgroundMesh = new Mesh();
    }
		backgroundMesh.Clear();
		List<Vector3> overlayVertices = new List<Vector3>();
		List<Vector2> overlayUv = new List<Vector2>();
		List<int> overlayTriangles = new List<int>();
		int overlayTriangleBase = 0;
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		int triangleBase = 0;
		for (int y = 0; y < MAP_HEIGHT; y++) {
			for (int x = 0; x < MAP_WIDTH; x++) {
        byte tile = TileTypeAt(x, y);
				if (tile == 0) continue;
        Vector2 uvPos;
        Vector2 overlayUvPos = Vector2.zero;
        bool overlay = false;
        if (tile == 1) {
          int bits = 0;
          if (TileTypeAt(x, y-1) != 1) bits |= 8;
          if (TileTypeAt(x-1, y) != 1) bits |= 4;
          if (TileTypeAt(x+1, y) != 1) bits |= 2;
          if (TileTypeAt(x, y+1) != 1) bits |= 1;
          if (bits == 0) {
            bits = 16;
            if (TileTypeAt(x-1, y-1) != 1) bits |= 8;
            if (TileTypeAt(x+1, y-1) != 1) bits |= 4;
            if (TileTypeAt(x-1, y+1) != 1) bits |= 2;
            if (TileTypeAt(x+1, y+1) != 1) bits |= 1;
          }
          uvPos = roadUV[bits];
        } else if (tile < 65) {
          uvPos = otherTiles[tile - 2];
        } else {
          overlay = !IsSolid(x, y);
          int paletteIndex = (tile - 65) / 22;
          if (overlay) {
            overlayUvPos = housePalettes[paletteIndex][(tile - 65) % 22];
            uvPos = otherTiles[0];
          } else {
            uvPos = housePalettes[paletteIndex][(tile - 65) % 22];
          }
        }
        if (overlay) {
          overlayVertices.Add(new Vector3(x, -y, 0));
          overlayVertices.Add(new Vector3(x + 1.05f, -y, 0));
          overlayVertices.Add(new Vector3(x + 1.05f, -y - 1.05f, 0));
          overlayVertices.Add(new Vector3(x, -y - 1.05f, 0));
          overlayUv.Add(new Vector2(overlayUvPos.x,             overlayUvPos.y + tileSizeV));
          overlayUv.Add(new Vector2(overlayUvPos.x + tileSizeH, overlayUvPos.y + tileSizeV));
          overlayUv.Add(new Vector2(overlayUvPos.x + tileSizeH, overlayUvPos.y));
          overlayUv.Add(new Vector2(overlayUvPos.x,             overlayUvPos.y));
          overlayTriangles.Add(overlayTriangleBase);
          overlayTriangles.Add(overlayTriangleBase+1);
          overlayTriangles.Add(overlayTriangleBase+2);
          overlayTriangles.Add(overlayTriangleBase);
          overlayTriangles.Add(overlayTriangleBase+2);
          overlayTriangles.Add(overlayTriangleBase+3);
          overlayTriangleBase += 4;
        }
        vertices.Add(new Vector3(x, -y, 0));
        vertices.Add(new Vector3(x + 1.05f, -y, 0));
        vertices.Add(new Vector3(x + 1.05f, -y - 1.05f, 0));
        vertices.Add(new Vector3(x, -y - 1.05f, 0));
        uv.Add(new Vector2(uvPos.x,             uvPos.y + tileSizeV));
        uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y + tileSizeV));
        uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y));
        uv.Add(new Vector2(uvPos.x,             uvPos.y));
        triangles.Add(triangleBase);
        triangles.Add(triangleBase+1);
        triangles.Add(triangleBase+2);
        triangles.Add(triangleBase);
        triangles.Add(triangleBase+2);
        triangles.Add(triangleBase+3);
        triangleBase += 4;
			}
		}
		backgroundMesh.vertices = vertices.ToArray();
		backgroundMesh.uv = uv.ToArray();
		backgroundMesh.triangles = triangles.ToArray();
		overlayMesh.vertices = overlayVertices.ToArray();
		overlayMesh.uv = overlayUv.ToArray();
		overlayMesh.triangles = overlayTriangles.ToArray();
	}

	public void Update() {
    Graphics.DrawMesh(backgroundMesh, new Vector3(0, 0, TileMap.BACKGROUND_DEPTH), Quaternion.identity, backgroundMaterial, 0, null, 0, null, false, false, false);
    Graphics.DrawMesh(overlayMesh, new Vector3(0, 0, TileMap.TERRAIN_DEPTH), Quaternion.identity, material, 9, null, 0, null, false, false, false);
	}
}
