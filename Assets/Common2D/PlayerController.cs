﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : CharacterCore
{
  private const float TERMINAL_VELOCITY = 20f;

  public AudioClip fireSound, hurtSound, teleportSound;
  private AudioSource thrustAudioSource;
  private float debounceTimer = -1f;
  public GameObject bulletPrefab;
  public GameObject teleportPrefab;
  public Texture2D radarIcon;
  public Sprite bulletIcon;

  [HideInInspector]
  public Vector2 velocity = new Vector2(0, 0);

  [HideInInspector]
  public float horizontal_momentum = 0f;

  [HideInInspector]
  public float vertical_momentum = 0f;

  private float deathTime = -1f;
  private float blinkTime = 0f;
  private float fireTime = 0f;
  private float chargeLevel = 0f;
  private bool showRadar = false;
  private float teleportTime = 0f;
  private GameObject teleportEffect = null;

  public int HorizontalDirection
  {
    get
    {
      float raw = Input.GetAxisRaw("Horizontal");
      if (Mathf.Abs(raw) < 0.2) return 0;
      return raw < 0 ? -1 : 1;
    }
  }

  public int VerticalDirection
  {
    get
    {
      float raw = Input.GetAxisRaw("Vertical");
      if (Mathf.Abs(raw) < 0.2) return 0;
      return raw < 0 ? -1 : 1;
    }
  }

  public Vector2 FindStartPoint()
  {
    // TODO: Application-specific
    return new Vector2(65, 65.5f);
  }

  public override void OnGameStart()
  {
    GameManager.lives = 3;
    tileMap.NewLevel();
    SetFacing(Vector2.up, false);
    chargeLevel = .5f;
    showRadar = false;
  }

  public override void OnLevelStart()
  {
    GameManager.instance.StartLife();
    SetFacing(Vector2.up, false);
  }

  public override void OnPlayerSpawn()
  {
    debounceTimer = Time.time + 0.25f;
    deathTime = 0;
    origin = FindStartPoint();
    velocity = Vector2.zero;
    GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", 0);
    GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", 0);
    if (chargeLevel < .5f) chargeLevel = .5f;
    showRadar = true;
    SetFacing(Vector2.up, false);

    GameManager.instance.StartLife();
  }

  public override void OnLifeStart()
  {
    InfestationManager.Purge(this);
  }

  public override void OnPlayerDeath()
  {
    deathTime = 1f;
    gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Dead", true);
    gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Fail");
    audioSource.PlayOneShot(deathSound);
  }

  public override void Start()
  {
    base.Start();
    origin = FindStartPoint();

    SetFacing(Vector2.up, false);
  }

  public void Update()
  {
    if (GameManager.state == GameState.Dead) {
      showRadar = false;
      teleportTime = 0;
      if (teleportEffect != null) {
        Destroy(teleportEffect);
        teleportEffect = null;
      }
      deathTime -= Time.deltaTime;
      if (deathTime > 0) {
        return;
      }
      if (Input.GetButtonDown("Fire") || GameManager.lives == 0) {
        GameManager.instance.SpawnLife();
      }
      return;
    }
    if (GameManager.state != GameState.Playing) {
      return;
    }

    if (showRadar) {
      chargeLevel -= Time.deltaTime * .02f;
      if (chargeLevel < 0) {
        chargeLevel = 0;
        showRadar = false;
      } else {
        GetComponent<SpriteAnimationManager>().PlayOnce("Channel");
      }
    } else if (chargeLevel < 4 && fireTime <= 0 && teleportTime <= 0) {
      chargeLevel += Time.deltaTime * .2f;
      if (chargeLevel > 4) chargeLevel = 4;
    }

    if (isInfested) {
      GameManager.instance.SetHealth(GameManager.health - Time.deltaTime * .3f);
    }

    if (blinkTime > 0) {
      blinkTime -= Time.deltaTime;
      if (blinkTime <= 0 || Mathf.Floor(blinkTime * 20f) % 2 == 0) {
        GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0f);
      } else {
        GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0.4f);
      }
    }

    if (Time.time > debounceTimer && GameManager.health > 0 && fireTime <= 0) {
      DoAction();
      DoMovement();
    } else {
      showRadar = false;
    }
    if (fireTime > 0) {
      fireTime -= Time.deltaTime;
      chargeLevel -= Time.deltaTime / 1.5f;
    }

    if (teleportTime > 0) {
      teleportTime -= Time.deltaTime;
      chargeLevel -= Time.deltaTime * 2;
      if (teleportEffect == null) {
        teleportEffect = Instantiate(teleportPrefab, transform);
      }
      if (teleportTime > .5f && teleportTime <= .75f) {
        teleportEffect.transform.localPosition = new Vector3(.3f, 1.9f, -1f);
      } else if (teleportTime > 0) {
        if (teleportTime > .4f) {
          teleportEffect.transform.localPosition = Vector3.Lerp(new Vector3(.3f, 1.9f, -1f), new Vector3(0f, 0f, -1f), 16 * (.5f - teleportTime));
        } else {
          teleportEffect.transform.localPosition = Vector3.Lerp(new Vector3(0f, .2f, -1f), new Vector3(0f, 0f, -1f), (.4f - teleportTime));
          teleportEffect.transform.localScale = Vector3.one * (.4f + (.5f - teleportTime) * 6f);
        }
      } else {
        if (isInfested) {
          PulseBehaviour pulse = bulletPrefab.gameObject.GetComponent<PulseBehaviour>();
          Instantiate(pulse.hazardPrefab, transform.position, Quaternion.identity);
          InfestationManager.Purge(this);
        }
        Destroy(teleportEffect);
        teleportEffect = null;
        Vector2 destination;
        do {
          destination = new Vector2(UnityEngine.Random.Range(1, TileMap.MAP_WIDTH - 1), UnityEngine.Random.Range(1, TileMap.MAP_HEIGHT - 1));
        } while (BlockedAt(destination));
        origin = destination;
      }
    } else if (teleportEffect != null) {
      Destroy(teleportEffect);
      teleportEffect = null;
    }
  }

  public void DoMovement()
  {
    if (showRadar || teleportTime > 0) return;

    int xAxis = HorizontalDirection;
    int yAxis = VerticalDirection;

    horizontal_momentum = Utility.Approach(horizontal_momentum, (Input.GetAxisRaw("Horizontal") * speed), 0.4f);
    velocity.x = horizontal_momentum;

    vertical_momentum = Utility.Approach(vertical_momentum, (Input.GetAxisRaw("Vertical") * -speed), 0.4f);
    velocity.y = vertical_momentum;

    SetFacing(velocity, xAxis != 0 || yAxis != 0);

    velocity = Move(velocity);
  }

  private void DoAction()
  {
    if (GameManager.state != GameState.Playing) {
      showRadar = false;
      return;
    }
    if (Input.GetButtonDown("Fire") && chargeLevel > 1) {
      fireTime = 1.5f;
      SetFacing(Vector2.up, false);
      gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Activate");
      audioSource.PlayOneShot(fireSound);
      Instantiate(bulletPrefab, transform.position + new Vector3(.3f, 1.9f, -1f), Quaternion.identity, transform);
    }
    if (Input.GetButtonDown("Teleport") && chargeLevel > 2) {
      GetComponent<SpriteAnimationManager>().PlayOnce("Teleport");
      audioSource.PlayOneShot(teleportSound);
      teleportTime = 1f;
    }
    if (Input.GetButtonDown("Radar")) {
      //chargeLevel -= Time.deltaTime * 5;
      showRadar = true;
    } else if (!Input.GetButton("Radar")) {
      showRadar = false;
    }
  }

  public override Vector2 Move(Vector2 v)
  {
    Vector2 result = base.Move(v);
    return result;
  }

  private int lastHeight = 0;
  public void LateUpdate()
  {
    Camera overlay = GameObject.FindWithTag("OverlayCamera").GetComponent<Camera>();
    Vector3 cameraPos = transform.position;
    if (Screen.height != lastHeight) {
      lastHeight = Screen.height;
      float divisor = 0f;
      float vBlocks;
      do {
        divisor += 1f;
        vBlocks = Mathf.Floor(Screen.height / 16.0f / divisor);
      } while (vBlocks > 20);
      Camera.main.orthographicSize = Screen.height / 16.0f / divisor / 2f;
      overlay.orthographicSize = Camera.main.orthographicSize;
    }
  }

  private static Dictionary<int, Texture2D> gaugeTextures = new Dictionary<int, Texture2D>();
  private Texture2D DrawGauge(float fill)
  {
    int frame = Mathf.FloorToInt(fill * 180f);
    if (!gaugeTextures.ContainsKey(frame)) {
      int size = (int)(Screen.height * .05f);
      Texture2D tex = new Texture2D(size, size, TextureFormat.ARGB32, false);
      int halfSize = size / 2;
      int r2 = halfSize * halfSize;
      int r2inner = (int)((halfSize - 1.5f) * (halfSize - 1.5f));
      for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++) {
          int distSquared = (x - halfSize) * (x - halfSize) + (y - halfSize) * (y - halfSize);
          if (distSquared > r2) {
            tex.SetPixel(x, y, new Color(0, 0, 0, 0));
          } else if (distSquared > r2inner) {
            tex.SetPixel(x, y, new Color(0, 0, 0, 1));
          } else {
            float angle = Mathf.Atan2(x - halfSize, y - halfSize) / (Mathf.PI * 2);
            if (angle < 0) angle += 1;
            if (angle < fill || fill == 1f) {
              tex.SetPixel(x, y, new Color(0, .87f, .87f, 1));
            } else {
              tex.SetPixel(x, y, new Color(0, 0, 0, 1));
            }
          }
        }
      }
      tex.Apply();
      gaugeTextures.Add(frame, tex);
      return tex;
    }
    return gaugeTextures[frame];
  }

  public void OnGUI()
  {
    float size = Screen.height * .05f;
    float xStep = (1.2f * size) / Screen.width;
    float xOffset = .99f - xStep;
    for (int i = 0; i < 4; i++) {
      GameManager.DrawTexture(DrawGauge(chargeLevel - i), xOffset - i * xStep, .002f, .05f, .05f);
    }

    if (showRadar) {
      float cost = InfestationManager.infestedChars.Count * Time.deltaTime * .01f;
      if (chargeLevel < cost) {
        showRadar = false;
        return;
      }
      chargeLevel -= cost;
      Vector3 center = transform.position - Vector3.up;
      foreach (CharacterCore character in InfestationManager.infestedChars) {
        if (character == this) continue;
        Rect bounds = new Rect(0, 0, 32f, 32f);
        Vector3 offset = (character.transform.position - transform.position);
        offset.y *= -1;
        float dist = Mathf.Clamp((offset.magnitude - 15f) / 40f, 0f, 1f) + 1f;
        bounds.center = Camera.main.WorldToScreenPoint(center + offset.normalized * dist);
        GUI.DrawTexture(bounds, radarIcon, ScaleMode.ScaleToFit);
      }
    }
  }

  public void PlayHurtSound()
  {
    audioSource.PlayOneShot(hurtSound);
  }
}
