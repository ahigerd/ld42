using UnityEngine;
using System;

public class Hazard : EnemyCore {
  public float age = 0;
  private ParticleSystem ps;
  public Vector2 trajectory = Vector2.one * 10;
  public CharacterCore source = null;
  private static CullingGroup cullingGroup = null;
  private static BoundingSphere[] spheres = new BoundingSphere[200];
  private static Hazard[] hazards = new Hazard[200];
  private static int numSpheres = 0;
  private BoundingSphere sphere;
  private int sphereIndex = 0;

  public static void ClearHazards() {
    if (cullingGroup == null) {
      cullingGroup = new CullingGroup();
      cullingGroup.targetCamera = Camera.main;
      cullingGroup.onStateChanged += Hazard.OnStateChanged;
      cullingGroup.SetBoundingSpheres(spheres);
    }
    cullingGroup.SetBoundingSphereCount(0);
    numSpheres = 0;
  }

  public static void Shutdown() {
    if (cullingGroup != null) {
      cullingGroup.Dispose();
      cullingGroup = null;
    }
  }

  public override void Start() {
    base.Start();
    ps = gameObject.GetComponent<ParticleSystem>();
    isInfested = true;
    disableCollision = true;
    if (numSpheres == spheres.Length) {
      Array.Resize(ref spheres, spheres.Length + 100);
      Array.Resize(ref hazards, spheres.Length);
    }
    sphereIndex = numSpheres;
    hazards[sphereIndex] = this;
    spheres[sphereIndex] = sphere = new BoundingSphere(transform.position, 1.25f);
    numSpheres++;
    cullingGroup.SetBoundingSphereCount(numSpheres);
  }

  void Update() {
    var main = ps.main;
    if (GameManager.IsRunning())
      age += Time.deltaTime;

    if (age < 2) {
      main.startColor = new Color(age * .5f, 0, 0, 1);
    }

    if (age <= 1) {
      Move(Vector2.Lerp(trajectory, Vector2.zero, age));
    } else {
      disableCollision = false;
    }

    float growthRate = age < 10 ? Time.deltaTime * .05f : Time.deltaTime * .01f;

    var startSpeed = main.startSpeed;
    startSpeed.constant -= growthRate;
    main.startSpeed = startSpeed;

    var vel = ps.velocityOverLifetime;
    vel.xMultiplier += 3 * growthRate;
    vel.yMultiplier += 3 * growthRate;

    var emission = ps.emission;
    if (age < 25) {
      emission.rateOverTime = 10 + 25 * (age / 10f);
    }

    var shape = ps.shape;
    float x = (1 - main.startSpeed.constant) * .1f;
    shape.position = new Vector3(x, (x + .5f) * 1.01f, shape.position.z);
    hitBoxSize = new Vector2(x * 10.7f, x * 11.77f);
    sphere.radius = hitBoxSize.y / 2f + .5f;

    if (!disableCollision) {
      foreach (CharacterCore character in InfestationManager.allChars) {
        // There's probably a cleverer mathy way to do this but I'm running out of time
        if (character.hitBox.Overlaps(hitBox)) {
          Vector2 offset = (character.transform.position - transform.position).normalized * PIXEL * .5f;
          character.Move(offset);
          Vector2 pos = character.origin;
          while (character.BlockedAt(new Vector2(pos.x, pos.y - .3f))) {
            // zip out of walls :(
            if (pos.x < 2) {
              pos.y += 1;
              if (character.origin.y >= TileMap.MAP_HEIGHT - 2) {
                pos.y -= 1;
                pos.x += 1;
              }
            } else {
              pos.x -= 1;
            }
          }
          character.origin = pos;
        }
      }
    }
  }

  private static void OnStateChanged(CullingGroupEvent evt) {
    if (evt.hasBecomeInvisible || evt.hasBecomeVisible) {
      Hazard hazard = hazards[evt.index];
      ParticleSystem ps = hazard.GetComponent<ParticleSystem>();
      Renderer[] rs = hazard.GetComponentsInChildren<Renderer>();
      if (evt.isVisible) {
        ps.Play(true);
        foreach (Renderer r in rs) {
          r.enabled = true;
        }
      } else {
        ps.Pause(true);
        foreach (Renderer r in rs) {
          r.enabled = false;
        }
      }
    }
  }
}
