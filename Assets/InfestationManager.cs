using UnityEngine;
using System.Collections.Generic;

public class InfestationManager : LevelAwareBehaviour
{
  static public InfestationManager instance = null;
  public ParticleSystem infestationEffect = null;
  public List<GameObject> npcPrefabs = new List<GameObject>();
  static public List<CharacterCore> allChars = new List<CharacterCore>();
  static public List<CharacterCore> cleanChars = new List<CharacterCore>();
  static public List<CharacterCore> infestedChars = new List<CharacterCore>();

  void Awake() {
    instance = this;
  }

  public override void OnGameStart() {
    base.OnGameStart();
    Hazard.ClearHazards();
  }

  void OnApplicationQuit() {
    Hazard.Shutdown();
  }

  public override void OnLevelStart() {
    TileMap map = GameObject.Find("Map").GetComponent<TileMap>();
    System.Random rnd = new System.Random();

    allChars.Clear();
    cleanChars.Clear();
    infestedChars.Clear();

    CharacterCore player = GameObject.FindWithTag("Player").GetComponent<CharacterCore>();
    allChars.Add(player);
    cleanChars.Add(player);

    for (int i = 0; i < 100; i++) {
      GameObject npc = Object.Instantiate(npcPrefabs[rnd.Next(npcPrefabs.Count)]);
      CharacterCore character = npc.GetComponent<CharacterCore>();
      allChars.Add(character);
      cleanChars.Add(character);
      do {
        float y = -rnd.Next(TileMap.MAP_HEIGHT) + .5f;
        npc.transform.position = new Vector3(rnd.Next(TileMap.MAP_WIDTH) + .5f, y, y / 100000f);
      } while (map.IsSolid(npc.transform.position.x, -npc.transform.position.y));
    }

    for (int i = 0; i < 20; i++) {
      // The player is always index 0
      int target = rnd.Next(cleanChars.Count - 1) + 1;
      CharacterCore npc = cleanChars[target];
      Infest(npc);
    }
  }

  public static void Infest(CharacterCore character) {
    if (character.isInfested || !character.isInfectious) {
      return;
    }
    cleanChars.Remove(character);
    infestedChars.Add(character);
    Object.Instantiate(instance.infestationEffect.gameObject, character.gameObject.transform);
    character.isInfested = true;
    PlayerController player = character.GetComponent<PlayerController>();
    if (player != null) {
      player.PlayHurtSound();
    }
  }

  public static void Purge(CharacterCore character) {
    if (!character.isInfested) {
      return;
    }
    infestedChars.Remove(character);
    cleanChars.Add(character);
    Object.Destroy(character.GetComponentInChildren<ParticleSystem>());
    character.isInfested = false;

    if (infestedChars.Count == 0) {
      GameManager.instance.EndGame();
    }
  }
}
